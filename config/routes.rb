Rails.application.routes.draw do
  # root path
  root to: 'books#index'

  # authentication
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }

  scope '/admin' do
    
    resources :users, except: [:show]
  	
  	resources :books
  	put 'books/:id/toogle_like', to: 'books#toogle_like'
  	
  	get 'manage_books', to: 'books#manage_books', as: 'manage_books'
  end
end
