FactoryGirl.define do
  factory :book do
    title { Faker::Book.title }
  	description { Faker::Movie.quote }
  	author { Faker::Book.author }
  	image_url { "https://placehold.it/250x250.png" }
  end
end
