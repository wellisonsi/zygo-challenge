require 'rails_helper'

RSpec.describe BooksController, type: :controller do
	context "GET #index" do
	  context "when visitant is not logged" do
	    it "responds not authorized" do
	      get :index

	      expect(response).to have_http_status(302)
	    end
	  end

	  context "when visitant is logged" do
	    it "responds successfully" do
	      user = create(:user)
	      sign_in user

	      get :index

	      expect(response).to have_http_status(200)
	    end
	  end
  end

  context "GET #manage_books" do
    context "when visitant is not librarian" do
      it "responds not authorized" do
        user = create(:user)
        sign_in user

        get :manage_books

        expect(response).to have_http_status(401)
      end
    end

    context "when visitant is librarian" do
      it "responds successfully" do
        user = create(:user, :librarian)
        sign_in user

        get :manage_books

        expect(response).to have_http_status(200)
      end
    end
  end

  context "POST #create" do
    context "when current_user is librarian" do
      before do 
        user = create(:user, :librarian)
        sign_in user
      end
      
      it "with valid params" do
        book_params = attributes_for(:book)

        expect{
          post :create, params: {book: book_params}
        }.to change{Book.count}.by(1)
      end

      it "with invalid params" do
        book_params = attributes_for(:book, title: nil)

        expect{
          post :create, params: {book: book_params}
        }.to_not change{Book.count}
      end
    end

    context "when current_user is not librarian" do
      it "returns not authorized" do
        user = create(:user)
        sign_in user

        book_params = attributes_for(:book)
        post :create, params: {book: book_params}
        
        expect(response).to have_http_status(401)
      end
    end
  end

  context "PUT #toogle_like" do
    context "when current_user is setted" do
      before do 
        @user = create(:user)
        sign_in @user
      end

      context "when has valid book params" do
        let!(:book) { create(:book) }

        it "creates favorite register" do

          expect{
            put :toogle_like, params: {id: book.id}
          }.to change{UserBook.where(user: @user, book: book).count}.by(1)
        end

        it "removes favorite register" do
          user_book = create(:user_book, user: @user)

          expect{
            put :toogle_like, params: {id: user_book.book_id}
          }.to change{UserBook.where(user: user_book.user, book: user_book.book).count}.by(-1)
        end
      end

      context "when does not has valid book params" do 
        it "raise error" do

          expect{
            put :toogle_like, params: {id: 99}
          }.to raise_error(ActiveRecord::RecordNotFound)
        end
      end
    end
  end
end
