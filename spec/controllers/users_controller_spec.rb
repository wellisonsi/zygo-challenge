require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  context "GET #index" do
    context "when visitant is not authorized" do
      it "responds not authorized" do
        get :index

        expect(response).to have_http_status(302)
      end
    end

    context "when visitant is librarian" do
      it "responds successfully" do
        user = create(:user, :librarian)
        sign_in user

        get :index

        expect(response).to have_http_status(200)
      end
    end
  end

  context "POST #create" do
    context "when current_user is librarian" do
      before do 
        user = create(:user, :librarian)
        sign_in user
      end

      it "with valid params" do
        user_params = attributes_for(:user)

        expect{
          post :create, params: {user: user_params}
        }.to change{User.count}.by(1)
      end

      it "with invalid params" do
        user_params = attributes_for(:user, email: nil)

        expect{
          post :create, params: {user: user_params}
        }.to_not change{User.count}
      end
    end

    context "when current_user is not librarian" do
      it "returns not authorized" do
        user = create(:user)
        sign_in user

        user_params = attributes_for(:user)
        post :create, params: {user: user_params}
        
        expect(response).to have_http_status(401)
      end
    end
  end
end
