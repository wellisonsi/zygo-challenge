require 'rails_helper'

RSpec.describe UserBook, type: :model do
  describe "associations" do
    it { is_expected.to belong_to(:book) }
    it { is_expected.to belong_to(:user) }
  end

  describe "validations" do 
    context "uniqueness of user_id" do
      subject { create(:user_book) }
      
      it { is_expected.to validate_uniqueness_of(:book_id).scoped_to(:user_id) }
    end
  end
end
