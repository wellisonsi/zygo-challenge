require 'rails_helper'

RSpec.describe User, type: :model do
  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:email) }
  end

  describe "associations" do
    it { is_expected.to have_many(:user_books) }
    it { is_expected.to have_many(:books) }
  end

  context "#liked?" do 
    let!(:user) { create(:user) }
    let!(:book) { create(:book) }

    context "when has favorited book" do
      it "returns true" do
        create(:user_book, user: user, book: book)

        expect(user.liked?(book)).to eq(true)
      end
    end

    context "when does not has favorited book" do
      it "returns false" do
        
        expect(user.liked?(book)).to eq(false)
      end
    end
  end

  context "when kind is not setted" do
  	it "set reader user" do 
  		user = create(:user)

  		expect(user.kind).to eq("reader")
  	end
  end

  context "when kind is setted to librarian" do
  	it "set librarian role to user" do 
  		user = create(:user, :librarian)

  		expect(user.kind).to eq("librarian")
  	end
  end

  context "when the email already exists" do
  	it "returns RecordInvalid error" do 
  		create(:user, email: "wellison.maceio@gmail.com")

  		expect{
  			create(:user, email: "wellison.maceio@gmail.com")
  		}.to raise_error(ActiveRecord::RecordInvalid)
  	end
  end
end
