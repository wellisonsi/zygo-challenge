require 'rails_helper'

RSpec.describe Book, type: :model do
  describe "validations" do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.to validate_presence_of(:author) }
  end

  describe "associations" do
    it { is_expected.to have_many(:user_books) }
    it { is_expected.to have_many(:users) }
  end

  describe "scopes" do
    describe ".title_like" do
      it "returns only with title partial text" do
        book1 = create(:book, title: "zygo versao 1")
        book2 = create(:book, title: "pagseguro e zygo")
        book3 = create(:book, title: "rails 2020")

        expect(described_class.title_like("zygo")).to match_array([book1, book2])
      end
    end

    describe ".description_like" do
      it "returns only with description partial text" do
        book1 = create(:book, description: "zygo versao 1")
        book2 = create(:book, description: "pagseguro e zygo")
        book3 = create(:book, description: "rails 2020")

        expect(described_class.description_like("zygo")).to match_array([book1, book2])
      end
    end

    describe ".author_like" do
      it "returns only with author  partial text" do
        book1 = create(:book, author: "zygo versao 1")
        book2 = create(:book, author: "pagseguro e zygo")
        book3 = create(:book, author: "rails 2020")

        expect(described_class.author_like("zygo")).to match_array([book1, book2])
      end
    end
  end

  describe "when image url is invalid" do
    it "raise error" do 
      expect{
        create(:book, image_url: "http://wwww.google.com")
      }.to raise_error(ActiveRecord::RecordInvalid, "A validação falhou: Imagem (URL) URL informada não possui uma imagem válida")
    end
	end

  describe "when image is invalid" do
    it "raise error" do 
      expect{
        create(:book)
      }.to change{Book.count}.by(1)
    end
  end
end
