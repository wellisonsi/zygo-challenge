# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.create(
  name: "Zygo Bibliotecario", 
  email: "bibliotecario@zygo.com.br", 
  kind: :librarian,
  password: "123456", 
  password_confirmation: "123456")

6.times do
  Book.create(
    title: Faker::Book.title,
    description: Faker::Movie.quote,
    author: Faker::Book.author,
    image_url: "https://placehold.it/250x250.png")
end
