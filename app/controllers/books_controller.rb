class BooksController < ApplicationController
  before_action :authenticate_user!
  before_action :check_librarian_access, except: [:index, :toogle_like]
  before_action :set_book, only: [:show, :edit, :toogle_like, :update, :destroy]

  # GET /books
  # GET /books.json
  def index
    @books = Book.all

    @books = @books.title_like(params[:title]) if params[:title].present?
    @books = @books.description_like(params[:description]) if params[:description].present?
    @books = @books.author_like(params[:author]) if params[:author].present?

    if params[:order].present? && params[:order] == "desc"
      @books = @books.order("title desc")
    else
      @books = @books.order("title asc")
    end
  end

  def manage_books
    @books = Book.all
  end

  # GET /books/1
  # GET /books/1.json
  def show
  end

  # PUT
  def toogle_like
    if current_user.liked?(@book)
      UserBook.where(user: current_user, book: @book).destroy_all
      render json: {liked: false}
    else
      UserBook.create(user: current_user, book: @book)
      render json: {liked: true}
    end
  end

  # GET /books/new
  def new
    @book = Book.new
  end

  # GET /books/1/edit
  def edit
  end

  # POST /books
  # POST /books.json
  def create
    @book = Book.new(book_params)

    respond_to do |format|
      if @book.save
        format.html { redirect_to manage_books_path, notice: 'Livro cadastrado com sucesso.' }
        format.json { render :show, status: :created, location: @book }
      else
        format.html { render :new }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /books/1
  # PATCH/PUT /books/1.json
  def update
    respond_to do |format|
      if @book.update(book_params)
        format.html { redirect_to manage_books_path, notice: 'Livro atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @book }
      else
        format.html { render :edit }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /books/1
  # DELETE /books/1.json
  def destroy
    @book.destroy
    respond_to do |format|
      format.html { redirect_to manage_books_path, notice: 'Livro removido com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.require(:book).permit(:title, :description, :author, :image_url)
    end
end
