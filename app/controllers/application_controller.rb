class ApplicationController < ActionController::Base
  protect_from_forgery

  before_action :configure_permitted_parameters, if: :devise_controller?

  def after_sign_in_path_for(resource)
    request.env['omniauth.origin'] || root_path
  end

  private

  def check_librarian_access
    return user_not_authorized unless current_user.librarian?
  end

  def user_not_authorized
    flash[:notice] = 'Você não tem permissão para executar esta ação'
    redirect_to books_url, :status => 401
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
  end
end
