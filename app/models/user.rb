class User < ApplicationRecord
  audited
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  enum kind: %i[librarian reader]

  after_initialize :set_default_kind

  validates :name, :email, presence: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP } 

  has_many :user_books, dependent: :destroy
  has_many :books, through: :user_books


  def liked?(book)
    user_books.where(book: book).any?
  end

  private

  def set_default_kind
    if self.new_record?
      self.kind ||= :reader
    end
  end

end
