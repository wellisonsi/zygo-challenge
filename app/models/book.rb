class Book < ApplicationRecord
  audited
  
  validates :title, :description, :author, presence: true
  validates :image_url, allow_blank: false, 
    format: { with: %r{.(gif|jpg|png)\Z}i, message: 'URL informada não possui uma imagem válida' }

  has_many :user_books, dependent: :destroy
  has_many :users, through: :user_books

  scope :title_like, ->(query) { where("title LIKE ?", "%#{query}%") }
  scope :description_like, ->(query) { where("description LIKE ?", "%#{query}%") }
  scope :author_like, ->(query) { where("author LIKE ?", "%#{query}%") }
  
end
