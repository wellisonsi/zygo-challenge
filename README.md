# Zygo Programming Challenge
#### Objetivo do desafio

Contruir um sistema simples de livros, onde usuários comuns podem acessar e buscar por livros.

### Tecnologias Utilizadas

* Ruby ruby 2.6.0
* Rails 5.2.2
* Postgres
* GIT para versionamento

### Instalação
* Configurar versão do Ruby on Rails de acordo com as especificações citadas na seção acima
* No arquivo database.yml inserir os dados de acesso ao banco de dados
* Dentro da pasta raiz do projeto executar os seguintes comandos
    * rails db:create
    * rails db:migrate
    * rails db:seed
* Após o processo acima, basta executar o comando **rails s** para subir a aplicação

### Funcionalidades Implementadas

Basicamente o sistema possui um fluxo de registro e login, onde um usuário que se registra pelo formulário de cadastro externo assume o papel no sistema de **leitor**
O perfil de Leitor tem acesso a visualizar os livros cadastrados, buscar livros por título, descrição e autor, ordernar a listagem por título e adicionar um livro a lista de favoritos
Ja o perfil de Bibliotécario, além dos acessos que o leitor possue, ele pode criar, editar e excluir Livros e usuários.

### Estrutura
Basicamente para completar o desafio, precisei apenas criar 3 entidades.
**User** -> Responsável por armazenar as informações de acesso e perfil do usuário
**Book** -> Responsável por armazenar os livros cadastrados
**UserBook** -> Responsável por armazenar os livros favoritados pelos usuários

Nota: A entidade User possui um campo chamado kind, que pode assumir os valores "reader"
ou "librarian", que respectivamentes, correspondem ao perfil de leitor e bibliotecário

Att,
Wellison Souza :)